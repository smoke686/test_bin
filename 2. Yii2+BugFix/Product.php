<?php

namespace test;

/**
 * Class Product
 *
 * @property integer $id                  Идентификатор элемента
 * @property string $title               Наименование
 * @property integer $is_published        Статус публикации
 * @property double $price               Стоимость
 * @property integer $user_id             Идентификатор пользователя
 * @property string $desc                Описание
 * @property string $created_at
 * @property string $updated_at
 *
 * @property string $searchKeyword       Поисковая фраза
 * @package test
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $searchKeyword;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'desc', 'price'], 'required', 'on' => self::SCENARIO_EDIT],
            [['price'], 'double'],
            [['is_published'], 'integer'],
            [['desc'], 'string'],
            [['created_at', 'updated_at'], 'save'],
        ];
    }

    /**
     * @param $params
     *
     * @return  yii\data\ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->joinWith(['user']);

        $dataProvider = new  yii\data\ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($this->validate()) {
            return $dataProvider;
        }

        $query->where([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'is_published' => $this->is_published,
            'title' => $this->title,
            'price' => $this->price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->where(['OR',
            ['like', 'user.name', $this->searchKeyword],
            ['like', 'user.surname', $this->searchKeyword],
            ['like', 'desc', $this->searchKeyword]
        ]);

        return $dataProvider;
    }

    /**
     * @param $id
     * @return null|Product
     */
    public static function findOnePublished($id)
    {
        return static::find()
            ->where(['id' => $id])
            ->andWhere(['is_published' => 1])
            ->one();
    }

    /**
     * Связь с пользователем
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasMany(User::class, ['user_id' => 'id']);
    }

    /**
     * Получаем массив товаров c указанием данных пользователя
     *
     * @return array
     */
    public function getUsersProducts(): array
    {
        $result = [];
        /** @var self[] $products */
        $products = self::find()->with(['user'])->all();
        foreach ($products as $product) {
            //Не очень понимаю зачем так написан код, почему просто не вернуть массив объектов?
            $result[] = [
                'id' => $product->id,
                'title' => $product->title,
                'price' => $product->price,
                'user' => [
                    //Юзера может не быть, добавил дефолтные значения
                    'id' => $product->user->id ?? '',
                    'name' => $product->user->name ?? '',
                    'surname' => $product->user->surname ?? '',
                ],
            ];
        }

        return $result;
    }

}