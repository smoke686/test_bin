<?php

namespace test;

/**
 * Class ProductController
 *
 * @package test
 */
class ProductController extends yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => '\yii\filters\VerbFilter',
                'actions' => [
                    //Удаление нужно через POST
                    //В теории можно создать XSS и человек случайно удалит запись
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Действие странички со списком товаров
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Product();
        $searchModel->setScenario(Product::SCENARIO_SEARCH);
        $dataProvider = $searchModel->search(\Yii::$app->request->post());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Действие странички с детальным описанием товара
     *
     * @param null|integer $id
     *
     * @return mixed
     */
    public function actionView($id = null)
    {
        $product = $this->findModelPublished($id);

        $this->view->title = 'Товар №' . $product->id;

        return $this->render('view', [
            'product' => $product,
        ]);
    }

    /**
     * Действие изменения товара
     *
     * @param $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = Product::findOne($id);
        $model->setScenario(Product::SCENARIO_EDIT);

        //if ($model->load(\Yii::$app->request->get()) && $model->save()) {
        //Нужно делать так, зачем вообще модель грузить гетом?
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            //Нужно делать так. Правила UrlManager могут измениться, и урл будет не верен
            return $this->redirect(['/default/update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Действие удаленения товара
     *
     * @param $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        //В данном случае модель может не найти и пытаться вызвать метод
        //Profile::findOne($id)->delete();
        //В данном случае, если модель не найдет, то вылезет 404
        $this->findModel($id)->delete();

        //Где метод list?
        //return $this->redirect(['list']);
        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     *
     * @return Product загрудженная модель
     * @throws NotFoundHttpException Если не найдем модель
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param integer $id
     *
     * @return Product загрудженная модель
     * @throws NotFoundHttpException Если не найдем модель
     */
    protected function findModelPublished($id)
    {
        if (($model = Product::findOnePublished($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}