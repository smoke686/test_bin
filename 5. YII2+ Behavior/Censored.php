<?php

namespace app\behaviors;


use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;


/**
 * Class Censored
 * Behavior который удаляет масстив стоп-слов из указанных атрибутов перед сохранением модели ActiveRecord
 *
 * Пример добавления в модель
 * public function behaviors()
 * {
 *      'censored' => [
 *          'class' => 'app\behaviors\Censored',
 *          'stopList' => ['слово', 'тест', 'test'],
 *          'attributes' => ['name'],
 *      ]
 * }
 *
 * @package app\behaviors
 */

class Censored extends Behavior
{
    public $stopList = [];
    public $attributes = [];

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'checkedAttributes',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'checkedAttributes'
        ];
    }

    /**
     * @param Event $event
     */
    public function checkedAttributes($event)
    {
        $owner = $this->owner;
        //Проходимся по атрибутам
        foreach ($this->attributes as $attribute) {
            //Если не существует атрибута, идем дальше
            if(!isset($owner->{$attribute})){
                continue;
            }
            //Проходимся по списку стоп-слов и удаляем их
            foreach ($this->stopList as $value){
                $owner->{$attribute} = str_replace($value, '', $owner->{$attribute});
            }
        }
    }
}