<?php

namespace test;

/**
 * Class Profile
 *
 * @property integer $rulerType
 * @property integer $profileType
 * @property string $fullName
 * @property string $birthDay
 * @property string $passportSN
 * @property string $passportIssuedDate
 * @property integer $passportIssuedCode
 * @property string $passportIssuedBy
 * @property integer $postalCode
 * @property string $cityName
 * @property string $streetName
 * @property integer $houseNumber
 * @property integer $buildingNumber
 * @property integer $flatNumber
 * @property string $position
 * @property boolean $isReady
 * @property integer $action
 *
 * @package test
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * Тип и гражданство: Физическое лицо резидент РФ
     */
    const PROFILE_TYPE_FL = 'fl';

    /**
     * Тип и гражданство: Физическое лицо (Другое)
     */
    const PROFILE_TYPE_FL_OTHER = 'flOther';

    /**
     * @var array $profileTypeList Варианты типов гражданств
     */
    public static $profileTypeList = [
        self::PROFILE_TYPE_FL => 'Физическое лицо резидент РФ',
        self::PROFILE_TYPE_FL_OTHER => 'Физическое лицо (Другое)',
    ];

    /**
     * @var array $rulerTypeList Варианты типов руководящего органа
     */
    public static $rulerTypeList = [
        1 => 'Высший орган оправления',
        2 => 'Исполнительный орган',
    ];

    /**
     * @var string Действие
     */
    public $action;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rulerType' => 'Руководящий орган',
            'profileType' => 'Тип и гражданство',
            'fullName' => 'ФИО',
            'birthDay' => 'Дата рождения',
            'passportSN' => 'Серия и номер паспорта',
            'passportIssuedDate' => 'Дата выдачи',
            'passportIssuedCode' => 'Код подразделения',
            'passportIssuedBy' => 'Кем выдан',
            'postalCode' => 'Индекс',
            'cityName' => 'Город/Населенный пункт',
            'streetName' => 'Улица',
            'houseNumber' => 'Дом',
            'buildingNumber' => 'Корпус',
            'flatNumber' => 'Квартира',
            'position' => 'Должность, наименование и реквизиты акта о назначении (избрании)',
            'isReady' => 'Готова к выгрузке',
            'action' => 'Действие',
        ];
    }
}