<?php
/**
 * @var $model Profile
 */

use test\Profile;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<?php $form = ActiveForm::begin([
    'action' => '/default/form/action',
    'options' => ['class' => 'reporting-data_form'],
    'fieldConfig' => [
        'template' => '{label} {input}',
        'options' => ['class' => 'form-field'],
        'labelOptions' => ['class' => 'form-label'],
        'inputOptions' => ['class' => 'form-input'],
    ],
]); ?>
<div class="reporting-data_row">
    <div class="row">
        <div class="col-3">
            <?= $form->field($model, 'rulerType')->dropDownList($model::$rulerTypeList, ['class' => 'js-dropdown-box', 'style' => 'display: none;']); ?>
        </div>

        <div class="col-9">
            <?= $form->field($model, 'profileType')->radioList($model::$profileTypeList, [
                'tag' => 'ul',
                'item' => function ($index, $label, $name, $checked, $value) {
                    return
                        '<li><label class="_link' . ($index == 0 ? ' active' : '') . '">' . Html::radio($name, $checked, ['value' => $value]) . $label . '</label></li>';
                },
            ]); ?>
        </div>
    </div>
</div>

<div class="reporting-data_row">
    <div class="reporting-data_title">
        Общая информация
    </div>

    <div class="row">
        <div class="col-8">
            <?= $form->field($model, 'fullName')->textInput() ?>
        </div>

        <div class="col-4">
            <?= $form->field($model, 'birthDay', ['options' => ['class' => 'form-field form-field--date']])->textInput(['class' => 'form-input js-simple-datepicker']) ?>
        </div>
    </div>
</div>

<div class="reporting-data_row">
    <div class="reporting-data_title">
        Данные документа, удостоверяющего личность
    </div>

    <div class="row">
        <div class="col-3">
            <?= $form->field($model, 'passportSN')->textInput() ?>
        </div>

        <div class="col-3">
            <?= $form->field($model, 'passportIssuedDate', ['options' => ['class' => 'form-field form-field--date']])->textInput(['class' => 'form-input js-simple-datepicker']) ?>
        </div>

        <div class="col-3">
            <?= $form->field($model, 'passportIssuedCode')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <?= $form->field($model, 'passportIssuedBy')->textInput() ?>
        </div>
    </div>
</div>

<div class="reporting-data_row">
    <div class="reporting-data_title">
        Адрес
    </div>

    <div class="row">
        <div class="col-4">
            <?= $form->field($model, 'postalCode')->textInput() ?>
        </div>

        <div class="col-4">
            <?= $form->field($model, 'cityName')->textInput() ?>
        </div>

        <div class="col-4">
            <?= $form->field($model, 'streetName')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-4">
            <?= $form->field($model, 'houseNumber')->textInput() ?>
        </div>

        <div class="col-4">
            <?= $form->field($model, 'buildingNumber')->textInput() ?>
        </div>

        <div class="col-4">
            <?= $form->field($model, 'flatNumber')->textInput() ?>
        </div>
    </div>
</div>

<div class="reporting-data_row">
    <div class="row">
        <div class="col-12">
            <div class="form-field">
                <?= Html::label($model->getAttributeLabel('position')
                    . Html::tag('i', '', ['class' => 'info-btn', 'title' => 'Всплывающий комментарий']),
                    null, ['class' => 'form-label']); ?>
                <?= Html::activeTextarea($model, 'position', ['class' => 'form-area', 'title' => $model->getAttributeLabel('position')]); ?>
            </div>
        </div>
    </div>
</div>

<div class="reporting-footer">
    <div class="reporting-footer_left">
        <?= $form->field($model, 'isReady', ['options' => ['class' => 'check-box'], 'template' => "{input}\n{label}"])
            ->checkbox(['class' => 'check-input', 'uncheck' => null, $model->isReady], false)
            ->label(null, ['class' => 'check-label']) ?>
    </div>
    <ul class="reporting-footer_right">
        <li>
            <?= Html::button('Сохранить как черновик', ['name' => 'action', 'value' => 'save', 'type' => 'submit', 'class' => 'btn btn-outline-primary btn-edit']) ?>
        </li>
        <li>
            <?= Html::button('Отправить', ['name' => 'action', 'value' => 'save', 'type' => 'submit', 'class' => 'btn btn-primary']) ?>
        </li>
    </ul>
</div>
<?php ActiveForm::end(); ?>
